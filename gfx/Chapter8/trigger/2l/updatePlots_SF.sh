
address='http://www.hephy.at/xuser/dspitzbart/TopEFT/trigger/2l_OR_combined_altBinning_ttW/'


declare -a files=("lep_pt[0]_Run2016_2l" 
                  "lep_pt[1]_Run2016_2l"
                  "lep_pt[0]_Run2016_2m"
                  "lep_pt[1]_Run2016_2m"
                  "lep_pt[0]_Run2016_2e"
                  "lep_pt[1]_Run2016_2e"
                  "lep_pt[0]_Run2016_em"
                  "lep_pt[1]_Run2016_em"
                  "lep_pt[0]_Run2017_2l" 
                  "lep_pt[1]_Run2017_2l"
                  "lep_pt[0]_Run2017_2m"
                  "lep_pt[1]_Run2017_2m"
                  "lep_pt[0]_Run2017_2e"
                  "lep_pt[1]_Run2017_2e"
                  "lep_pt[0]_Run2017_em"
                  "lep_pt[1]_Run2017_em"
                 )


for i in "${files[@]}"
do
  newFileName=$i
  newFileName+="_SF"
  echo $newFileName
  if [ -f $newFileName.pdf ]; then
    rm $newFileName.pdf
  fi
  if [ -f $newFileName.root ]; then
    rm $newFileName.root
  fi
  wget $address/$i.pdf
  wget $address/$i.root
  mv $i.pdf $newFileName.pdf
  mv $i.root $newFileName.root
done
