
address='http://www.hephy.at/xuser/dspitzbart/TopEFT/trigger/TTLep_pow_17/turnOn_3l_MET_HTMHT_JetHT_altBinning/'

declare -a channels=("2l" 
                     "2e"
                     "em" 
                     "2m"
                    )

declare -a files=("lep_pt[0]_comp_zoom" 
                  "lep_pt[1]_comp_zoom"
                  "lep_eta[0]_comp_zoom" 
                  "lep_eta[1]_comp_zoom"
                 )


for j in "${channels[@]}"
do
  for i in "${files[@]}"
  do
    channel=$i
    channel+="_"
    channel+=$j
    channel+="_Fall17"
    newFileName=$channel
    echo $newFileName
    if [ -f $newFileName.pdf ]; then
      rm $newFileName.pdf
    fi
    if [ -f $newFileName.root ]; then
      rm $newFileName.root
    fi
    wget $address/$j/$i.pdf
    wget $address/$j/$i.root
    mv $i.pdf $newFileName.pdf
    mv $i.root $newFileName.root
  done
done
