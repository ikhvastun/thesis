
address='http://www.hephy.at/xuser/dspitzbart/TopEFT/trigger/MET_Run2017/turnOn_3l_MET_HTMHT_JetHT_altBinning/3pl/'

declare -a files=("lep_pt[0]_comp_zoom" 
                  "lep_pt[1]_comp_zoom"
                  "lep_pt[2]_comp_zoom"
                  "lep_eta[0]_comp_zoom" 
                  "lep_eta[1]_comp_zoom"
                  "lep_eta[2]_comp_zoom"
                 )

for i in "${files[@]}"
do
  if [ -f $i.pdf ]; then
    rm $i.pdf
  fi
  if [ -f $i.root ]; then
    rm $i.root
  fi
  wget $address/$i.pdf
  wget $address/$i.root
done
