
address='http://www.hephy.at/xuser/dspitzbart/TopEFT/trigger/3l_OR_combined_altBinning_SF_v2/'

declare -a files=("lep_pt[0]_Run2017" 
                  "lep_pt[1]_Run2017"
                  "lep_pt[2]_Run2017"
                  "nElectrons_Run2017"
                  "nElectrons_Run2016"
                 )

for i in "${files[@]}"
do
  if [ -f $i.pdf ]; then
    rm $i.pdf
  fi
  if [ -f $i.root ]; then
    rm $i.root
  fi
  wget $address/$i.pdf
  wget $address/$i.root
done
