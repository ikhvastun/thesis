%************************************************
\chapter{Introduction}\label{ch:introduction}
%************************************************

%One of the most indispensable human needs stranding on the top of Maslow's pyramid is the need to recognize. This need led us into the search and exploration of the universe around us. The knowledge we gained tell us that the building blocks of the nature around us are particles which are bound by fundamental forces. Even though we already reached quite an advanced level of understanding of our universe, I can guarantee: the universe is still full of mysteries.

The Standard Model (\acs{SM}) of particle physics is one of the most successful theories of the last century. It summarizes our understanding of all known elementary particles and forces; many predictions of this theory have been confirmed in precision experiments over the past decades. The discovery of the \ac{BEH} boson in $2012$ by the \acs{CMS} and \acs{ATLAS} Collaborations was the culminating achievement of the \acs{SM}~\cite{Chatrchyan:2012xdj,Aad:2012tfa}. While the \acs{SM} is renormalizable and could be consistent theory valid up to possibly Planck scale which describes current experimental data quite well, there are several phenomena it cannot satisfactorily accommodate such as the non-baryonic dark matter, the origin of matter-antimatter asymmetry, and the non-zero neutrino masses. 

\hide{
the indication of the dark matter content in the universe, the matter-antimatter asymmetry and neutrino oscillations are among the phenomena the \acs{SM} cannot predict. Additionally, it should be mentioned that the \acs{SM} covers only three of the four fundamental forces of nature, namely the electromagnetic, weak and strong interactions. Gravity, the dominant force at large scales, is simply absent in the \acs{SM}. Even though the \acs{SM} has succeeded in predicting and explaining quite a wide range of physics processes, it can not be the ultimate theory describing the universe.
}

Supersymmetry (\acs{SUSY}) is often considered as an attractive solution to several outstanding problems of the \acs{SM}. The large data volumes of proton-proton collisions at the unprecedented center-of-mass energy ($\sqrt{s}$) reached by the Large Hadron Collider (\acs{LHC}) provide a unique opportunity to search for new physics in general. If \acs{SUSY} is realized in nature near the weak scale, the \acs{LHC} is bound to discover it. Nonetheless, so far analyzed \acs{LHC} data shows no sign of new physics. Even though a large volume of data sets from \acs{LHC} still needs to be analyzed, these searches are not expected to extend the discovery reach of the \acs{LHC} significantly. Alternatively, other strategies might have to be exploited to find new physics. One possibility is to consider a model-independent approach and to search for new physics through precision measurements of processes at the electroweak scale, which involve heavy particles such as \acs{BEH}, W and Z bosons and top quarks. \textbf{In this thesis we present searches for new physics exploring both methodologies; direct search for \acs{SUSY} and precision measurements in the top quark sector.} 

\hide{
The first part of this thesis is dedicated to the searches of strong \acs{SUSY} production in the events with at least 3 electrons or muons, jets, and missing transverse momentum. No significant excess above the expected \acs{SM} background is observed in the data set collected in 2016~\cite{Sirunyan:2017hvp}, therefore, if \acs{SUSY} exists, the mass of the particles should be much larger than the TeV scale. In the near future, no significant increase in energy of the proton beams and in the amount of collected data are anticipated\footnote{estimated amount of data that will be collected in phase I of Large Hadron Collider, i.e. from 2015 till 2022, is approximately 10 times larger than the data collected in 2016}, therefore to find physics beyond the \acs{SM} we might need to reevaluate our strategy. One possibility is to consider a model-independent approach and to search for new physics in abundantly occurring electroweak-scale processes, which involve recently discovered heavy particles such as \acs{BEH}, W and Z bosons and top quarks.
}

The analysis concerning search for \acs{SUSY} used the data set from \acs{LHC} run in 2015 and 2016 and led to stringent constraints on the masses of the \acs{SUSY} particles such as gluinos, 3rd generation squarks, and lightest \acs{SUSY} particles - neutralinos.

Using the data collected in 2016 and 2017 we performed cross section measurements for top pair production in association with massive vector bosons (\ttW, \ttZ). Several dedicated techniques were developed to increase the precision in the measurements. Most important ones are the improved electron and muon identification using machine learning techniques, which led to significantly increased signal purity, and precise determination of backgrounds due to misidentified leptons using data control samples. The results of this study present the most precise measurements of these processes available to date. In particular, the precision reached in \ttZ is for the first time better than the state-of-the-art theoretical calculations estimated at next-to-leading order quantum chromodynamics and electroweak accuracy. The methodologies developed within the context of this analysis were also applied in other measurements performed by the UGent \acs{CMS} group, such as the observation of single top production in association with a Z boson~\cite{Sirunyan:2018zgs}. These measurements provide a solid ground for other precision studies at the electroweak scale, where the differential distributions can be studied in detail and can be used in the context of Effective Field Theory (\acs{EFT}) to constrain new physics at scales higher than the range probed with direct searches at the \acs{LHC}.  

\hide{
The relatively high production rate of top pair in association with massive vector bosons (\ttV, V = W or Z) allowed to study them precisely, based on the data sample collected at the \acs{LHC} Run II. The second part of the thesis is dedicated to the inclusive cross section measurement for \ttW and \ttZ processes. Both of processes were discovered by \acs{CMS} and \acs{ATLAS} Collaborations in the \acs{LHC} Run I; however, the achieved precision was at the level of $25$-$30$\%~\cite{Khachatryan:2015sha, Aad:2015eua}. The data sample collected during the Run II allows studying these processes with a largely improved precision. The first step in the exploring of the \ttV processes in Run II was their re-discovery at $13$ TeV center-of-mass energy data~\cite{Sirunyan:2017uzs}. This measurement had also a major impact on discovery of other important processes in top quark sector, such as single top production in association with a Z boson and top pair production with a \acs{BEH} boson. These measurements provide a solid ground for other precision studies at the electroweak scale, such as inclusive and differential cross sections of the \ttZ process, which represents one of the topics in this PhD thesis.
}

In summary, this thesis gives a comprehensive overview of the studies performed between 2015 and 2019 using data collected by the \acs{CMS} experiment in the proton-proton collision at $\sqrt{s} = $ 13 TeV. The thesis starts with a theoretical overview of the \acs{SM} in Chapter $2$. Chapter $3$ introduces the \acs{LHC} and the \acs{CMS} detector components including trigger and online processing. Chapter $4$ describes Monte Carlo (\acs{MC}) simulation models and programs used in this thesis for collision event simulation. Object identification and event reconstruction are presented in Chapter $5$. Chapter 6 summarizes physics analysis techniques for the \acs{SUSY} searches in events with multiple charged leptons, jets, and missing transverse momentum. These measurements have been published in the peer-reviewed \acs{CMS} collaboration papers:

\begin{itemize}
	\item CMS Collaboration, "Search for supersymmetry with multiple charged leptons in proton-proton collisions at $\sqrt{s}$ = $13$ TeV", \textbf{Eur. Phys. J. C $77$ ($2017$) $635$}
	\item CMS Collaboration, "Search for supersymmetry in events with at least three electrons or muons, jets, and missing transverse momentum in proton-proton collisions at $\sqrt{s}$ = $13$ TeV", \textbf{JHEP $02$ ($2018$) $067$}
\end{itemize}

Preliminary results of these studies are also published as a \acs{CMS} public note:

\begin{itemize}
	\item CMS Collaboration, "Search for SUSY with multileptons in $13$ TeV data", Physics Analysis Summary, \textbf{CMS-SUS-$16$-$022$}.
\end{itemize}

Chapter 7 summarizes the measurement of top pair production in association with a W boson, using the 2016 dataset collected by the \acs{CMS} experiment; the results are published:

\begin{itemize}
	\item CMS Collaboration, "Measurement of the cross section for top quark pair production in association with a W or Z boson in proton-proton collisions at $\sqrt{s}$ = $13$ TeV", \textbf{JHEP $08$ ($2018$) $011$}
\end{itemize}

Preliminary results of this measurement are also available as a \acs{CMS} public note: 

\begin{itemize}
	\item CMS Collaboration, "Measurement of the top pair-production in association with a W or Z boson in proton-proton collisions at $13$ TeV", Physics Analysis Summary, \textbf{CMS-TOP-$16$-$017$}.
\end{itemize}

Chapter 8 presents the results of the cross section measurement of top pair production in association with a Z boson. The results of these measurement are in preparation to be submitted for publication. The preliminary results are published in a physics analysis summary by the CMS collaboration:

\begin{itemize}
	\item CMS Collaboration, "Measurement of top quark pair production in association with a Z boson in proton-proton collisions at $\sqrt{s}$ = 13 TeV", Physics Analysis Summary, \textbf{CMS-TOP-$18$-$009$}.
\end{itemize}

Finally, a summary and prospects for future analyses are given in Chapter 10.

The results obtained in this thesis were presented by the PhD candidate in various international conferences: 

\begin{itemize}
	\item "Search for SUSY with multileptons in proton-proton collisions in 13 TeV center-of-mass energy data using CMS detector", poster presented at the \textbf{General Scientific Meeting of the Belgian Physical Society}, May $2016$, Ghent. 
	\item ”Studies of tt+V processes in the CMS Experiment”, talk at the \textbf{Top Quark Physics workshop}, September 2016, Olomouc, Czech Republic. This preliminary result was presented the co-called ICHEP dataset, representing about 30\% of the total statistics available by the end of the 2016. In addition, new measurement on the top pair production with a photon at 8 TeV center-of-mass energy was also reported.
	\item “Studies of the tt+X in CMS”, talk at the \textbf{Large Hadron Collider Physics Conference}, May 2017, Shanghai, China. This talk outlined first CMS preliminary result on the measurement of top pair production with a vector boson, using the whole 2016 dataset. Additionally, the most recent results were presented for the measurement of various processes in top quark sector, among which are the top pair production with a photon, top pair production with a pair of b quarks and four top pair production.
	\item  ”Measurement of the top quark pair-production in association with a W or Z boson in pp collisions at $13$ TeV with full $2016$ dataset at CMS experiment”, talk at the \textbf{La Thuile Conference}, March $2018$, La Thuile, Italy. This talk was given during the Young Scientific Forum, where young researchers can deliver their results to the broad particle physics community.
\end{itemize}
