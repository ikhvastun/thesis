%************************************************
\chapter{The CMS experiment at the LHC}\label{ch:lhcandcms} 
%************************************************
%************************************************

In order to study the physics described in Chapter~\ref{ch:theory}, we need a particle accelerator which provides us with high-energy proton-proton collisions, in which the physics processes of interest are produced. The proton-proton collision events provided by the Large Hadron Collider (\acs{LHC})~\cite{Bruning:782076}, the most powerful collider built by human, are used in the study presented in this thesis. The \acs{LHC} machine details will be described in the first part of this chapter. Smashing particles in the laboratory has its benefit: we can control what happens at the collision point of the beams by reconstructing the collision products. For this purpose, we use the \ac{CMS}~\cite{Chatrchyan:2008aa} detector, which is described in the second part of this chapter.

Colliders may either be ring accelerators or linear accelerators, and may collide a single beam of particles against a stationary target or two beams head-on. When two beams collide, the center-of-mass energy of the collision is the sum of their energies. A beam of the same energy that hits a fixed target would produce a collision of much less energy which is proportional to square root of the energy beam in the center-of-mass frame. A linear particle accelerator is a type of particle accelerator that accelerates charged subatomic particles by putting them to a series of oscillating electric potentials along a linear beam line. The most famous one in this category is the \ac{SLC} accelerator, that collided electrons and positrons with the center-of-mass energy close to the Z boson mass, thereby investigating its properties. In a circular accelerator, particles are accelerated in a circle until they reach sufficient energy. The advantage of circular accelerators over linear accelerators is that the ring topology allows continuous acceleration, as the particle can transit indefinitely. Besides the \acs{LHC}, other remarkable circular accelerators were the Tevatron, which discovered the top quark in $1995$, and KEKB, which until $2018$ had the world's highest luminosity record. Besides these colliders, another remarkable and unique accelerator was hosted by DESY in Hamburg. HERA was an electron-proton accelerator, which had the investigation of the proton structure as its main task.

To accelerate a particle, it has to have an electromagnetic charge, which can be used to accelerate and focus the beam of the particles. Another important feature is that the particle should be stable enough so it will not decay during the acceleration time. Among all particles, the best candidates for the accelerators are electrons, protons, and heavy nuclei. The proton has its internal structure, it consists of so-called partons, which are carrying part of the total energy of the proton. Because the initial momenta of the partons are not fixed values, proton-proton interactions can be used to probe a broad range of energies. Thus proton-proton colliders can be used as discovery machines. On the contrary, various experiments show that there is no structure for the electron. This means we know the initial energy of interaction in electron colliders with extreme precision. 

Of course, nothing comes without a price: in circular colliders, particles are bend by magnets to stay inside the collider trajectory. When a particle changes trajectory it loses energy due to the so-called synchrotron radiation or bremsstrahlung. The intensity of this radiation is inversely proportional to the fourth power of the mass of the particles, which means electrons lose significantly larger amount of energy than protons of the same energy. For example, the last largest electron-positron collider - \acs{LEP} - had severe constraints on the possible maximum energy of the electrons. At the end of the \acs{LEP}’s run, each revolution caused electrons to lose approximately $3\%$ of their energy~\cite{Brandt:2000xk}. This constraint kept \acs{LEP} from discovering the \acs{BEH} boson, which was found by the \acs{ATLAS} and \acs{CMS} Collaborations in $2012$. 

\section{LHC}

To obtain protons for further acceleration, hydrogen gas is injected into a metal cylinder to break down the gas into protons and electrons. The protons are then sent to the linear accelerator LINAC $2$, where they are accelerated to $50$ MeV and then injected into the Proton Synchrotron Booster, which accelerates them up to about $1.4$ GeV. At the next step, protons are transferred to the Proton Synchrotron, which increases their energy up to $25$ GeV and afterward the Super Proton Synchrotron ring brings the protons energy up to $450$ GeV. The LHC is the last ring in a complex chain of accelerators, where proton bunches reach the energy of $6.5$ TeV and cause them to collide head-on, creating center-of-mass energies of $13$ TeV. The entire accelerator complex is shown in Fig.~\ref{fig:CERN}.

\hide{
Subsequently, protons go into Proton Synchrotron, which pushes the beam energy up to $25$ GeV. Lastly they are sent to the Super Proton Synchrotron and after it the proton beam is ready to be transferred to the LHC at energy of $450$ GeV. The protons are injected into two counter rotating beams in the \acs{LHC} which accelerates each proton up to $6.5$ TeV. The entire complex used to accelerate the protons is shown in Fig.~\ref{fig:CERN}.
}

\begin{figure}[bth]
        %\centering
        \includegraphics[width=\textwidth]{gfx/Chapter3/CCC-v2017-croped.png}
        \caption{Schematic representation of the CERN accelerator complex and experiments(taken from~\cite{Mobs:2197559}).}\label{fig:CERN}
\end{figure}

In the accelerator, particles circulate in a vacuum and are steered using electromagnetic field: dipole magnets keep particles at their nearly circular orbits, quadrupole magnets focus the beam, and accelerating cavities are electromagnetic resonators that accelerate particles and keep them at the fixed energy by compensating for energy losses. Protons in phase with the electric field are accelerated, while the protons out of phase with the field are decelerated; this mechanism allows to group protons into several bunches. The LHC uses eight cavities per beam, each 
capable of delivering $5$ MV/m accelerating field at $40$ MHz, as a consequence these bunches are separated by a $25$ ns spacing. The $15$-meters long dipole magnets are based on superconducting technology and designed to generate a high field of $8.3$ T. In order to achieve the strength of the magnetic field the niobium-titanium (NbTi) cables are used, which are operated at a temperature of $1.9$ K. In \acs{LHC}, there are $1232$ dipole and $400$ quadrapole magnets, which under nominal operating conditions transport $2\times2808$ bunches in the opposite directions, each containing $10^{11}$ protons. 

\subsection{Luminosity}

The beams are colliding at several interaction points along the \acs{LHC}; apart from \acs{CMS}, these are \acs{ATLAS}, \acs{ALICE} and \acs{LHCb} experiments. The number of interactions that effectively takes place at these collision points is determined by the instantaneous luminosity, $L$, which is defined as the ratio of the number of events detected in a certain time period to the interaction cross section:

\begin{equation}
L = \frac{1}{\sigma}\frac{dN}{dt}
\end{equation}

Integrating this over time gives a related quantity - integrated luminosity:

\begin{equation}
\mathcal{L} = \int L dt
\label{eq:TotalLumi}
\end{equation}

These parameters are useful to characterize the performance of an accelerator. Once the integrated luminosity is known, the expected number of events of a given type, having certain cross section, $\sigma$, is calculated as: 

\begin{equation}
N = \sigma\mathcal{L}
\end{equation}

If two bunches containing $n_{1}$ and $n_{2}$ protons collide head-on with frequency, $f$, an instantaneous luminosity can be expressed as:

\begin{equation}
L = f \frac{n_1 n_2}{4\pi\sigma_{x}\sigma_{y}},
\end{equation}

where $\sigma_{x}$ and $\sigma_{y}$ characterize transverse beam sizes in the horizontal and vertical directions.

In the second run period (Run II) the \acs{LHC} reached its peak instantaneous luminosity of about $2.1\times 10^{34} cm^{-2}s^{-1}$, which is higher than any previous machine operating at the energy frontier. The LEP, for instance, had an instantaneous luminosity of about $2\times 10^{31} cm^{-2}s^{-1}$~\cite{ALEPH:2005ab} while the Tevatron reached $4\times 10^{32} cm^{-2}s^{-1}$ colliding protons and antiprotons. In $2018$ the \acs{LHC} had overcome the record of the highest instantaneous luminosity out of any accelerator; the previous one was held by the KEKB for almost $10$ years~\cite{Abe:2013kxa}.

As it can be seen from Eq.~\ref{eq:TotalLumi} the instantaneous luminosity can be described as the "data collection speed". As an example, the ZEUS detector at HERA accelerator was operating from $1992$ to $2007$. During these $15$ years, it collected $0.5$ \fbinv of data~\cite{ZEUSlumi}, which is an amount of the data collected by the \acs{CMS} detector each operational day in $2018$~\cite{CMSlumi}.

\subsection{Pile-up}

Every time two proton bunches collide, multiple proton-proton collision takes place. This effect is known as pile-up (\acs{PU}). The \acs{CMS} detector has to register decay products of unstable particles and reconstruct backward which particles correspond to different primary vertexes. The \acs{CMS} detector was designed to operate at an average number of $25$ interactions per crossing and with instantaneous luminosity of $\approx 1\times 10^{34} cm^{-2}s^{-1}$. Currently, this number has been already exceeded by factor $2$. Such an instantaneous luminosity causes an intense particle flux through the CMS sub-detectors and might cause severe radiation damage in the detector elements. The \acs{PU} distribution in the CMS detector is shown in Fig.~\ref{fig:pileup} (left) for the collisions which happened in $2018$. As an example, Fig~\ref{fig:pileup} (right) shows a recorded event that contained $78$ simultaneous proton-proton collisions in a single bunch crossing.

\begin{figure}[bth]
        \includegraphics[width=.5\linewidth]{gfx/Chapter3/pileup_pp_2018_69200.pdf}
        \includegraphics[width=.5\linewidth]{gfx/Chapter3/bunchCross78.png}
        \caption{Distribution of the number of proton-proton collisions per bunch crossing at the CMS interaction point during the $13$ TeV run (left)~\cite{CMSlumi}, and an event display of an 8 TeV event, where $78$ distinct vertexes were reconstructed. Data recorded on the $21$st of September $2012$ (right)~\cite{tooManyPU}.}\label{fig:pileup}
\end{figure}

%Every interaction originates from its own vertex, and therefore a good vertex reconstruction is needed in CMS detector for distinguishing particles coming from the primary interaction vertex, which is usually taken to be the one with the hardest interaction, from those coming from pileup vertices.

%During the last two years, improvements have been made to the track reconstruction software to ensure that CMS can continue to reconstruct tracks with high purity efficiently and quickly, even under conditions of very high pile-up. Following the track reconstruction, the tracks are grouped into vertices, each one representing a proton-proton collision. Since only a few collisions reveal interesting physics, being able to distinguish tracks from different vertices is essential to disentangle the effect of pile-up collisions from the collision of interest. The exceptional performance of the Tracker can be seen in Image 1: the event display shows 78 reconstructed vertices in one beam crossing, obtained from a special high-pileup run. Images 2 and 3 display only the side-on or Rho-Z view, which shows how closely spaced the 78 collisions are. More images of this collision event may be found on CDS.

%The LHC's extremely high luminosity gives a possibility to study very rare processes. As a drawback it leads to multiple proton-proton collisions per bunch crossing, which is called 

%The LHC’s extremely high luminosity has the distinct advantage that even very rare processes will occur, and can subsequently be studied. Such a luminosity is not all sunshine and rainbows though, as it leads to multiple proton-proton collisions per bunch crossing in an LHC interaction point. This phenomenon is dubbed pileup and means that interesting hard, non-difractive events will be accompanied by a lot of soft activity, and sometimes even other hard interactions. Every interaction originates from its own vertex, and therefore a good vertex reconstruction is needed in LHC detectors for distinguishing particles coming from the primary interaction vertex (usually taken to be the one with the hardest interaction) from those coming from pileup vertices. The distribution the number of interactions per bunch crossing in the CMS detector is shown in figure 4.4 for the previous 8 TeV run, together with a recorded event that contained 29 proton-proton collisions in a single bunch crossing.

\section{The CMS detector}

\acs{CMS} together with \acs{ATLAS} are two general purpose detectors specifically designed to study the \acs{BEH} boson and \acs{BSM} physics. The \acs{CMS} detector has a cylindrical shape, around \acs{LHC}'s beamline, consisting of a central barrel and two endcaps. CMS is $21.6$ m long, with a diameter of $15$ m. The central element of the CMS apparatus is a superconducting solenoid, providing a magnetic field of $3.8$ T. \acs{CMS} comprises of various sub-detectors and technologies, which are shortly described in this section. A silicon pixel and strip tracker, a lead tungstate (PbWO$_{4}$) crystal electromagnetic calorimeter (\acs{ECAL}), and a brass and scintillator hadron calorimeter (\acs{HCAL}) are all placed within the \acs{CMS} solenoid. Muons are detected in gas-ionization chambers embedded in the steel flux-return yoke outside the solenoid. A graphical view of \acs{CMS} with its sub-detectors is given in Fig.~\ref{fig:cms}. % This magnet is crucial for determining the charge and momentum of high energy particles by their deflection in its field. 

\begin{figure}[bth]
        \includegraphics[width=\linewidth]{gfx/Chapter3/cms_detector.png}
        \caption{Schematic view of the \acs{CMS} detector. The \acs{LHC} beams travel in opposite directions along the central axis of the \acs{CMS} cylinder colliding in the middle of the \acs{CMS} detector. The figure is taken from~\cite{CMSDetector}.}
        \label{fig:cms}
\end{figure}

\subsection{CMS coordinate system}

\acs{CMS} uses a right handed Cartesian coordinate system where the origin is at the nominal collision point inside \acs{CMS}. The z-axis points along the tangent to the counter-clockwise rotating proton beam, the x-axis is pointing to the center of the \acs{LHC} and y-axis pointing up perpendicular to the \acs{LHC} plane. These coordinate positions are indicated in Fig.~\ref{fig:cmsCoord}.

\begin{figure}[bth]
        \includegraphics[width=\linewidth]{gfx/Chapter3/cms_coordinate_system.png}
        \caption{Coordinate system used by the \acs{CMS} experiment at the \acs{LHC}. Here Jura indicates a direction of a sub-alpine mountain range located north of the Western Alps. The Figure is made with a code taken from~\cite{CMScoordSystem}.}
        \label{fig:cmsCoord}
\end{figure}

The polar angle, $\theta$, is measured from the positive z-axis and the azimuthal angle, $\phi$, is measured from the positive x-axis in the x-y plane. One of the important quantities - pseudorapidity, $\eta$, is defined as

\begin{equation}
\notag
\eta = - \log\left(\tan\left(\frac{\theta}{2}\right)\right)
\end{equation}

and often used instead of the polar angle. The reason for this is that the difference in rapidity between two particles, $\Delta\eta$, is invariant under longitudinal Lorentz boosts along the beam axis. The angular separation between two detected particles is usually expressed in terms of the longitudinal Lorentz boost invariant quantity $\Delta R$, defined as: 

\begin{equation}
\Delta R = \sqrt{(\Delta\phi)^2 + (\Delta\eta)^2}.
\end{equation}

Here $\Delta\phi$ is difference in $\phi$ coordinate between two particles and this quantity is essential to be invariant under longitudinal boosts since its defined in the plane orthogonal to the beam direction~\cite{Chatrchyan:2008aa}. As already mentioned, CMS detector has a cylindrical shape, so it's essential to use one of the variables from cylindrical coordinate system. The axial distance or radial distance, $r$, is picked as the Euclidean distance in the x-y plane.

The momentum of a particle can be decomposed into longitudinal, parallel to the beam axis, and a transverse, perpendicular to the beam axis in the x-y plane $\left(\pt = \sqrt{p_{x}^{2} + p_{y}^{2}}\right)$, components. When two partons collide at \acs{CMS}, the initial momentum is not known along the beam axis direction (the particle can go undetected inside the beam pipe), while the momentum balance in the transverse plane must remain zero. Using the latter fact, we define the momentum and energy in the transverse plane, \pt and \ET, respectively. Particles which leave the detector undetected could be revealed by the missing energy \ETmiss in the transverse plane; this could be for instance neutrinos. % Four-momentum of a particle can then be expressed using \pt, $\eta$, $\phi$ together with its mass or transverse energy.

\subsection{Detectors within CMS}

In the following, we describe the \acs{CMS} sub-detectors starting from the closest one to the collision point and then ordered by distance increasing from the interaction point.

\paragraph{Tracker}

The tracking detectors are dedicated to provide a precise and efficient measurement of the charged particle momentum and trajectories. In addition, high granularity of the tracking system allows to reconstruct precisely the primary interaction vertex. It is usually taken as the one corresponding to the hardest proton-proton interaction, and distinguished from those coming from \acs{PU} vertexes and secondary vertexes from decays of long-lived heavy particles. 

The tracker has a total length of $5.8$ m and a diameter of $2.5$ m and has a coverage in $|\eta|$ up to $2.5$. A schematic overview of its geometry is shown in Fig.~\ref{fig:cmsTracker}. Within a few cm from the interaction point, a silicon pixel detector is installed to cope with the high particle flux. Around the pixel detector, silicon micro-strips are used. When charged particles pass through these pixels and strips, they cause ionization in the silicon, creating numerous electron-hole pairs. When an electric field is applied, these electrons and holes in the silicon drift towards the electrodes, generating fast signal in the detector.

\begin{figure}[bth]
        \centering
        \includegraphics[width=.8\linewidth]{gfx/Chapter3/CMStracker.png}
        \caption{Schematic view of the CMS tracker in half of the $r$-$z$ plane. The center of the tracker, corresponding to the approximate proton-proton collision point, is indicated by a star. Black and blue lines show strip tracker modules. Red lines show the pixel modules. The meaning of all abbreviations is described in the text. The Figure is taken from~\cite{Viliani:2016eys}.}
        \label{fig:cmsTracker}
\end{figure}

%Every proton-proton interaction originates its own vertex, and therefore a good vertex reconstruction is needed in CMS detector to distinguish particles coming from the primary interaction vertex, which is usually taken to be the one with the hardest interaction, from those coming from pileup vertices. This is a task for the first layer of the tracking system - vertex detector. 

The pixel detector consists of three barrel layers at radii between $4.4$ and $10.2$ cm from the beam axis, and at $34.5$ and $46.5$ cm from the interaction point in forward regions. The size of the silicon pixels, $100$ $\times$ $150$ $\mu m^2$ in $r$-$\phi$ and z planes, is chosen to achieve an excellent spatial resolution ($10$ $\mu m$ in $r$-$\phi$ plane and $15$ $\mu$ along z-axis) and to keep the detector occupancy at a maximum of $1$\% and efficiency well above $99$\%. The pixel detector covers $\eta$ range up to $2.5$. The original pixel detector was designed to withstand instantaneous luminosity of $1\times 10^{34} cm^{-2}s^{-1}$, while the luminosities of $1.5\times 10^{34} cm^{-2}s^{-1}$ were already achieved in $2016$ and some performance degradation was observed with a time. Therefore, the decision was made to replace the pixel detector during the technical stop in the beginning of $2017$. The upgraded pixel detector has one more sensitive layer in both barrel and forward regions, being designed to operate at a particle flux up to $2\times 10^{34} cm^{-2}s^{-1}$~\cite{Chatrchyan:2014fea}. 

The strip tracker is composed of silicon micro-strip detector and is divided in an inner region, with $4$ barrel layers and $3$ endcap layers, and an outer region, with $6$ barrel layers and $9$ endcap disks. The modules in the inner rings (denoted as TIB and TID in Fig.~\ref{fig:cmsTracker}) and first four endcap modules (denoted as TEC) use $320$ $\mu m$ thick silicon sensors, while those in the outer rings (denoted as TOB) and the outer three endcap rings use silicon of $500$ $\mu m$ thickness. Additionally, some of the layers carry a second microstrip detector, tilted with respect to each other, in order to measure the second coordinate, respectively $z$ and $r$ for the barrel and endcap. Within a layer, each module is shifted slightly in $z$ or $r$ with respect to its adjacent modules, allowing for an overlap to avoid gaps in acceptance.

%The first layer around the interaction point is the tracking system, designed to provide a precise and efficient measurement of the charged particle trajectories emerging from the LHC collisions. The magnitude and direction of these curved trajectories allow us to deduce the momentum and charge of these particles. Furthermore, the tracker is able to reconstruct vertices with great precision, which is needed to identify the secondary vertex of the decays of long-lived heavy particles, but also helps in distinguishing tracks from the primary vertex of interest from the many tracks originating in pile-up events. Due to the high pile-up and a short time between bunch crossings, a high granularity and fast response is required to process the large number of tracks.

\paragraph{ECAL}

The CMS \acs{ECAL} is a compact, hermetic, fine-grain, homogeneous calorimeter made of lead tungstate scintillating crystals~\cite{CMS:1997ema}. The choice of lead tungstate was primarily motivated by its radiation tolerance, its small radiation length ($0.89$ cm) and Moliere radius\footnote{the radius of a cylinder containing $90$\% of the electromagnetic cascade's energy deposition}, and its fast response ($99$\% of the light is collected in $100$ ns, which is compatible with the $25$ ns bunch spacing at \acs{LHC}). The cylindrical barrel consists of $61200$ crystals and the \acs{ECAL} endcaps are made up of $15000$ crystals. The crystal length is $23$ cm in the barrel and $22$ cm in the endcap. When electrons or photons pass through the \acs{ECAL}, they collide with the nuclei of the crystals and deposit their energy by bremsstrahlung or pair production generating a scintillation light in the crystals. The scintillation light output is rather low, so highly efficient photodetectors are placed at the rear of the crystal to increase the current produced by scintillation light. 

The \acs{ECAL} is composed of a barrel, surrounding the tracker, and two endcap sections. The barrel part of the \acs{ECAL} covers a range in $\eta$ up to $1.479$ and the endcaps cover the rapidity range $1.479$ $<$ |$\eta$| $<$ $3.0$. The $1.653 < |\eta| < 2.6$ range is also equipped with a preshower detector with high granularity, able to distinguish a neutral pion decaying to photons from actual photons. The preshower has a much finer granularity than the \acs{ECAL} with detector strips of $2$ mm wide, compared to the $3$ cm wide ECAL crystals, and can register each of the pion-produced particles as a separate photon. The CMS \acs{ECAL} layout is shown in Fig.~\ref{fig:ECAL}.


\begin{figure}[bth]
        \centering
        \includegraphics[width=.8\linewidth]{gfx/Chapter3/ECAL.png}
        \caption{Geometric view of one-quarter of the \acs{ECAL} in the $y$-$z$ plane. The numbers on the plot reflect the position of the \acs{ECAL}s components as a function of $\eta$. The Figure is taken from~\cite{Benaglia:2014aqa}.}
        \label{fig:ECAL}
\end{figure}

The \acs{ECAL} energy resolution for electrons in barrel region has been measured in the beam tests and can be approximated as:

\begin{equation}
\frac{\sigma_{E}}{E} = \frac{2.8\%}{\sqrt{E (\text{GeV})}} \oplus \frac{12\%}{E (\text{GeV})} \oplus 0.3\%,
\end{equation}

where the first is a stochastic term responsible for intrinsic fluctuations from shower development, second is the noise term due to the electronic noise of the readout chain and third is the constant term that is usually caused by instrumental effects~\cite{Adzic:2007mi}.

\paragraph{HCAL}

The \acs{HCAL}’s purpose is to measure the energy of both neutral- and charged hadrons. When hadrons enter a medium they initiate a hadronic cascade, known as a shower, through strong interactions with the nuclei. These showers develop relatively slow compared to electromagnetic cascades, and even though the hadrons already lose some energy in the \acs{ECAL}, they will deposit most of it in the \acs{HCAL}. As opposed to the \acs{ECAL}, which is a homogeneous detector, the \acs{HCAL} is a sampling calorimeter which consists of alternating layers of high density absorber material and layer of plastic scintillators converting the absorbed energy into the light pulse.

The detector is designed to be as hermetic as possible and covers a range in $\eta$ up to $5.0$. This is to ensure there are no particles escaping from the detection which is essential for the calculation of \ETmiss. The \acs{HCAL} consists of four different detectors. Inside the magnetic coil are the barrel detector (HB), covering a range in $\eta$ up to $1.3$, and the endcap detector (HE) which covers the range $1.3 < \eta < 3.0$. The outer detector (HO) is an additional layer of scintillators placed outside of the magnet, in order to collect the energy from hadron showers penetrating through the barrel detector. Finally, coverage between $2.9 < |\eta| < 5.0$ is guaranteed by the HF detector, which detects particles through the emission of Cherenkov light in the absorber crystals. The geometry and location of the \acs{HCAL} sub-detectors is shown in Fig.~\ref{fig:HCAL}.

\begin{figure}[bth]
        \centering
        \includegraphics[width=.8\linewidth]{gfx/Chapter3/HCAL.png}
        \caption{Longitudinal view of the \acs{CMS} \acs{HCAL} detector showing the locations of the barrel (HB), endcap (HE), forward (HF) and outer (HO) detectors. The Figure is taken from~\cite{CMSEcalPic}.}\label{fig:HCAL}
\end{figure}

The energy resolution for the reconstructed jet in the \acs{HCAL} at energies around $30$ GeV is about $30$\%, and it improves for increasing energy to less than $10$\%, in all regions of the detector~\cite{Chatrchyan:2008aa}.

\paragraph{Magnet and return yoke}

The \acs{CMS} detector uses a solenoid magnet which is the core of the detector, and is also reflected in the name of the experiment. A good momentum resolution of the high momentum charged particles is achieved by applying a strong magnetic field and measuring the curvature of the charged particle track. The \acs{CMS} magnet is $6$ m in diameter and $12.5$ m in length and is made out of NbTi. The temperature of $4$ K provided by the cooling system ensures the magnet to stay in a "superconducting" regime, i.e. electricity flows without resistance inside the magnet, and creates a powerful $3.8$ T uniform magnetic field. The magnet return yoke of the CMS detector, confines the magnetic field and stops all remaining particles except for muons and neutrinos.

\paragraph{Muon system}

Muons can penetrate several meters of iron, losing their energy only via ionization, and contrary to other particles they are not absorbed by any of \acs{CMS} sub-detectors. Therefore, chambers to detect muons are placed at the very end of the \acs{CMS} detector. The muon system is composed of three types of gaseous detectors placed between the return yoke layers. 

When a muon traverses a gas chamber, it ionizes the gas creating electron-ion pairs. Applied voltage between electrodes creates an electric field, where electrons drift to the positively charged anode whereas ions slowly move to the negatively charged cathode. Close to anode surface, electron energy becomes sufficient to ionize other atoms in the gas. As a result the avalanche inside the gas volume is created, which amplifies the output signal.

%In the presence of an electric field, created by applying a voltage potential, these electrons drift to the positively charged anode whereas the ionized gas moves to the negatively charged cathode. If the electric field is strong enough, the electrons can ionize other atoms in the gas. This results in an electron avalanche which amplifies the current. The movement of the charges towards the anode induce an electric signal which can be read out.

In the barrel region ($|\eta| < 0.9$), where the muon rate is low and the magnetic field uniform, drift tubes (\acs{DT}s) are used. The \acs{DT}s are installed in the $4$ detector stations alternated with the segmented return yoke. Each station is arranged in $2$ or $3$ so-called "superlayers", each consisting of $4$ layers of \acs{DT}s. The "superlayers" are orthogonal to each other, in which each "superlayer" focuses on the measurement in the direction of $\phi$ or $z$.

\begin{figure}[bth]
        \centering
        \includegraphics[width=.8\linewidth]{gfx/Chapter3/muonSystem.jpg}
        \caption{The quadrant of the \acs{CMS} muon system in $r$-$z$ plane. The steel yoke is represented by darkly shaded blocks between the muon chambers. The \acs{DT}s are shown in light orange, \acs{CSC}s in green, \acs{RPC}s in blue and \acs{GEM}s in red. The "B" letter in the name indicates the chamber belongs to the barrel and "E" represents the endcap part. The Figure is taken from~\cite{Abbaneo:2014uxa}.}
        \label{fig:muonSystem}
\end{figure}

In the endcap region ($0.9 < |\eta| < 2.4$), where particle rates are higher and the magnetic field is non-uniform and large, cathode strip chambers (\acs{CSC}s) are used. \acs{CSC}s have a trapezoidal form and consist of a cathode plane divided into strips and multiple anode wires orthogonal to the cathode strips. The cathode strips measure the $\phi$ coordinate, while the anode wires are optimized for bunch crossing identification and measurement in the $\eta$ direction.

Resistive plate chambers (\acs{RPC}s) are used in both barrel and endcap regions up to $|\eta| < 2.1$. \acs{RPC}s consist of two gas gaps, one serves as anode and another as a cathode. Due to the electric field in the gap, an electron avalanche is created when an ionizing particle crosses the detector. The movement of the charges induces a signal on the read-out strips. \acs{RPC}s have a coarser position resolution compared with the other two muon detectors, but their fast response allows to use them as a trigger for muon identification.

A high-luminosity upgrade of the \acs{LHC} is foreseen during a third long shutdown, which starts in $2023$, to further increase the instantaneous luminosity to $5\times 10^{34} cm^{-2}s^{-1}$. To cope with this unprecedented luminosity the \acs{CMS} muon system must be able to withstand a physics program that maintains sensitivity for electroweak scale physics and for TeV scale searches. One of the future improvements in the muon system is the installation of additional layers of muon detectors which are based on gas electron multiplier (\acs{GEM}) technology in the first endcap muon station in order to maintain and improve the forward muon triggering and reconstruction in the region $1.6 < |\eta| < 2.2$. The proposed \acs{GEM} detectors satisfy all performance requirements and are able to operate at particle fluxes far above those expected in the forward region under high luminosity \acs{LHC} conditions~\cite{Colaleo:2015vsq}.

\section{Trigger system and data acquisition}

When \acs{CMS} is running at design luminosity, about one billion proton-proton interactions take place every second inside the detector. Such amount of data could not be processed and stored for all collisions. The vast majority of interactions are low-energy soft collisions, while interesting, energetic head-on collisions happen much less often. Therefore, \acs{CMS} designed a powerful multilayer trigger system to select interesting events.

The trigger system consists of two levels designed to select events corresponding to rare physics processes, e.g. \acs{BEH} and \acs{BSM} physics, from billions of proton-proton collisions. The first level of the trigger is implemented on a hardware level, and selects events containing detector signals consistent with an electron, photon, muon, $\tau$ lepton, jet, or \ETmiss signatures. It reduces the initial event rate to an output rate of $100$ kHz and has only $3.2$ $\mu$s to accept or reject an event. As it is impossible to readout full tracker in this short time, the first level trigger relies solely on the information from the muon system and the calorimeters.

If an event is accepted by the first level trigger, data from all sub-detectors are analyzed at the High Level Trigger (\acs{HLT}), where the event rate gets reduced to 1-2 kHz. The \acs{HLT} trigger algorithms are built up of a series of filters to make a decision on an event. To pass the trigger, an event needs to pass every filter in the corresponding trigger path. Events passing the \acs{HLT} are recorded permanently for further physics analysis~\cite{Khachatryan:2016bia}.

\section{Grid computing}

\acs{CMS} produces about five petabytes of data per year running at peak performance, even though this amount is already significantly decreased by the trigger system. To handle this amount of data, a distributed computing and data storage infrastructure called the Worldwide \acs{LHC} Computing Grid was developed~\cite{Shiers:2007eye}. The grid gets together tens of thousands of standard computers worldwide, giving access to data and simulation samples and analysis of the data to thousands of scientists all over the world. 