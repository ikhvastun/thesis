%********************************************************************
% Appendix
%*******************************************************
% If problems with the headers: get headings in appendix etc. right
%\markboth{\spacedlowsmallcaps{Appendix}}{\spacedlowsmallcaps{Appendix}}
\hide{
\chapter{Closure test in MC}\label{sec:appendixCTinsitu}

\begin{figure}[!hbtp]
\centering
        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_offZ/HT.pdf}
        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_offZ/met.pdf} \\

        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_offZ/njets.pdf}
        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_offZ/nbjets.pdf}\\

        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_offZ/pttrail.pdf}
        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_offZ/flavour.pdf}

\caption{Fake background estimation closure in \ttbar in the off-Z regions. The fake rate has been measured in a \ttbar \acs{MC} sample with an "in-situ" method described in Chapter~\ref{sec:SUSYbackgroundEst}. Shown are the distributions of the hadronic activity $\HT$, the missing transverse energy $\ETmiss$, the (b-)jet multiplicity $\Njets$ ($\Nbjets$), and the \pt distributions for the trailing leptons for events with muons and electrons. Additionally, the yields in events with different lepton flavor compositions is shown.}
\label{fig:closure_MuElOff_ttbar_insitu}
\end{figure}

\begin{figure}[!hbtp]
\centering
        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_onZ/HT.pdf}
        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_onZ/met.pdf} \\

        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_onZ/njets.pdf}
        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_onZ/nbjets.pdf}\\

        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_onZ/pttrail.pdf}
        \includegraphics[width=.45\textwidth]{gfx/Chapter6/nonpromptBackground/CT_insitu_onZ/flavour.pdf}

\caption{Fake background estimation closure in \ttbar in the on-Z regions. The fake rate has been measured in a \ttbar \acs{MC} sample with an "in-situ" method described in Chapter~\ref{sec:SUSYbackgroundEst}. Shown are the distributions of the hadronic activity $\HT$, the missing transverse energy $\ETmiss$, the (b-)jet multiplicity $\Njets$ ($\Nbjets$), and the \pt distributions for the trailing leptons for events with muons and electrons. Additionally, the yields in events with different lepton flavor compositions is shown.}
\label{fig:closure_MuElOn_ttbar_insitu}
\end{figure}
}

\chapter{Search for Supersymmetry in Events with Multiple Charged Leptons, Jets, and Missing Transverse Momentum}

\section{High-level trigger paths}

Table~\ref{tab:SUSYtriggers} lists the high-level trigger paths that are used to select signal events in \acs{SUSY} analysis. All of these dilepton triggers have not been prescaled during the relevant data taking period. Auxiliary single lepton triggers, which are listed in Table~\ref{tab:SUSYtriggersFR}, are used to select \acs{QCD} multijet events for measuring the lepton misidentification rate, which is used for the estimation of the nonprompt lepton background. The auxiliary triggers have been prescaled to reduce their rate.

\begin{table}[h!]
\begin{center}
\resizebox*{1\textwidth}{!}{
\begin{tabular}{|c|c|c|}
\hline
\hline
 &Channel & Trigger Name \\
\hline
\multirow{5}{*}{isolated} & $\mu\mu$ & HLT\_Mu17\_TrkIsoVVL\_(Tk)Mu8\_TrkIsoVVL\textcolor{blue}{(\_DZ)} \\ \cline{2-3}
& $ee$ & HLT\_Ele23\_Ele12\_CaloIdL\_TrackIdL\_IsoVL\_DZ\\ \cline{2-3}
& \multirow{2}{*}{$e\mu$} &HLT\_Mu8\_TrkIsoVVL\_Ele23\_CaloIdL\_TrackIdL\_IsoVL\textcolor{blue}{(\_DZ)}\\
& & HLT\_Mu23\_TrkIsoVVL\_Ele8\_CaloIdL\_TrackIdL\_IsoVL\textcolor{blue}{(\_DZ)}\\ \hline
\multirow{3}{*}{non-isolated} & $\mu\mu$ &HLT\_DoubleMu8\_Mass8\_PFHT300 \\\cline{2-3}
&$ee$  &HLT\_DoubleEle8\_CaloIdM\_TrackIdM\_Mass8\_PFHT300\\\cline{2-3}
&$e\mu$  &HLT\_Mu8\_Ele8\_CaloIdM\_TrackIdM\_Mass8\_PFHT300\\

\hline
\hline
\end{tabular}}
\end{center}
\caption{Set of triggers used in the SUSY analysis. The requirement on the distance between two leptons (indicated in blue in the names) is used only in the dataset collected in the latest run era and corresponds to approximately $9$ \fbinv of the total collected data in $2016$. For this period the triggers without mentioned requirement were prescaled.} \label{tab:SUSYtriggers}
\end{table}

\begin{table}[h!]
\begin{center}
\caption{\label{tab:SUSYtriggersFR} Auxiliary triggers used to select \acs{QCD} multijet events for measuring the lepton misidentification rate.}
\begin{tabular}{|c|c|}
\hline
\hline
Channel & Trigger Name \\
\hline

\multirow{2}{*}{$\mu$}                      & HLT\_Mu8 \\
                                            & HLT\_Mu17 \\
\hline
  \multirow{2}{*}{$e$}     & HLT\_Ele8\_CaloIdM\_TrackIdM\_PFJet30 \\
                           & HLT\_Ele17\_CaloIdM\_TrackIdM\_PFJet30 \\
\hline
\hline
\end{tabular}%}
\end{center}
\end{table}

\clearpage

\section{List of SM signal and background simulations}

Table~\ref{tab:SUSYsamples} lists all \acs{MC} simulations for \acs{SM} signal and background processes used in the SUSY analysis. The cross sections used for the normalizations of the background \acs{MC} samples are calculated from the matrix element of the \acs{MC} generator used. The cross sections for signal samples are calculated at NLO-NLL(next-to-leading-logarithm) accuracy and assumes that other SUSY particles are decoupled (i.e. very massive). The NNPDF3.0~\cite{Ball:2014uwa, Ball:2017nwa} \acs{PDF}s are used for the hard scattering process in the samples. The modeling of the underlying event is generated using the CUETP8M1~\cite{Skands:2014pea, Khachatryan:2015pea} for most of the samples in signal and background categories, apart from the \ttH sample where the CUETP8M2 tune~\cite{CMS:2016kle} is utilized.

The background simulations listed in the ‘nonprompt’ category are not used for the final result of the analysis where this type of background is estimated with a data-driven technique. Instead, these simulations are used for closure tests to validate the nonprompt lepton background estimation.

\begin{table}[bth]
\begin{center}
\resizebox{1.0\linewidth}{!}{
\begin{tabular}{|lllll|} \hline
Category                   & Sample Name                                             & Generator          & Matching scheme & $\sigma$ pb \\ \hline 
Signal                     & -                                                       & MADGRAPH5\_aMC@NLO & MLM             & -           \\ \hline         

\multirow{4}{*}{nonprompt} & \ttbar to $2\ell$                                       & Madgraph           & MLM             & $87.315$      \\
                           & \ttbar to $1\ell$                                       & Madgraph           & MLM             & $364.36$      \\
                           & DY to $2\ell$ ($\text m_{\ell\ell} > 50$ GeV)           & MADGRAPH5\_aMC@NLO & FxFx            & $6024$       \\
                           & DY to $2\ell$ ($10$ GeV $\text < m_{\ell\ell} < 50$ GeV)& MADGRAPH5\_aMC@NLO & FxFx            & $18610$       \\ \hline
WZ                         & WZ to $3\ell1\nu$                                       & Powheg             & -               & $4.4297$      \\ \hline
\multirow{2}{*}{\ttZ}      & \ttZ, Z to $2\ell$ or $2\nu$ ($\text m_{\ell\ell} > 10$ GeV) & MADGRAPH5\_aMC@NLO & -               & $0.2529$      \\
                           & \ttZ, Z to $2\ell$ ($1$ GeV $<\text m_{\ell\ell} < 10$ GeV)  & Madgraph           & MLM             & $0.0283$      \\ \hline
\multirow{7}{*}{\ttX}      & \ttW, W to $1\ell1\nu$                                  & MADGRAPH5\_aMC@NLO & FxFx            & $0.2043$      \\
                           & \ttH, H to all, except b$\bar{\text b}$                 & Powheg             & -               & $0.2151$      \\
                           & tZq, Z to $2\ell$                                       & MADGRAPH5\_aMC@NLO & -               & $0.0758$      \\
                           & tHq, H inclusive                                        & Madgraph           & -               & $0.261$       \\
                           & tHW, H inclusive                                        & Madgraph           & -               & $0.01561$       \\
                           & tWZ, Z to $2\ell$                                       & Madgraph           & -               & $0.01123$     \\
                           & $\ttbar\ttbar$                                          & MADGRAPH5\_aMC@NLO & -               & $0.009103$    \\
                           & $\ttbar\gamma$                                          & MADGRAPH5\_aMC@NLO & FxFx            & $3.697$       \\
                           & $t\gamma$                                               & MADGRAPH5\_aMC@NLO & -               & $2.967$       \\ \hline
\multirow{9}{*}{Rare SM}   & ZZ, both Z to $2\ell$                                   & Powheg             & -               & $1.256$      \\        
                           & HZZ, both Z to $2\ell$                                  & Powheg             & -               & $0.0121$      \\
                           & VH, H to all, except b$\bar{\text b}$                   & MADGRAPH5\_aMC@NLO & FxFx            & $0.9561$       \\
                           & ZZZ                                                     & MADGRAPH5\_aMC@NLO & -               & $0.01398$       \\
                           & WZZ                                                     & MADGRAPH5\_aMC@NLO & -               & $0.05565$       \\
                           & WWZ                                                     & MADGRAPH5\_aMC@NLO & -               & $0.1651$       \\
                           & WWW                                                     & MADGRAPH5\_aMC@NLO & -               & $0.2086$       \\        
                           & Z$\gamma$                                               & MADGRAPH5\_aMC@NLO & FxFx            & $131.3$      \\
                           & W$\gamma$                                               & MADGRAPH5\_aMC@NLO & MLM             & $585.5$      \\
\hline
\end{tabular}}
\end{center}
\caption{List of MC simulations of standard model background processes used in the SUSY analysis. Category name, sample name, generator name, the parton matching scheme and processes cross section are presented.\label{tab:SUSYsamples}
}
\end{table}

\clearpage

\section{Yields for MC simulation in each SR}
\label{app:SUSYMCyields}

\begin{table}[tbh!]
\begin{center}
\caption{Simulated yields for 35.9 \fbinv of the rare \acs{SM} backgrounds in the $23$ on-Z signal regions and the associated statistical uncertainty from the size of the samples. Shown uncertainty is \acs{MC} statistics.}
\label{tab:SUSYMCyieldsONZ}

\begin{tabular}{||c||c|c|c|c|c|}\hline

        & \ttZ              & \ttX             & WZ                & Rares              & total\\ \hline \hline

SR1a & $18.47\pm0.69$   & $12.22\pm0.27$   & $189.80\pm3.83$   & $20.88\pm1.35$   & $241.37\pm4.12$  \\ \hline
SR1b & $3.11\pm0.29$   & $1.82\pm0.16$   & $13.17\pm1.00$   & $2.65\pm0.26$   & $20.75\pm1.09$  \\ \hline
SR2a & $4.73\pm0.35$   & $2.41\pm0.12$   & $38.72\pm1.73$   & $4.03\pm0.35$   & $49.89\pm1.80$  \\ \hline
SR2b & $1.05\pm0.18$   & $0.51\pm0.08$   & $2.03\pm0.40$   & $0.57\pm0.12$   & $4.17\pm0.46$  \\ \hline
SR3a & $4.48\pm0.39$   & $1.41\pm0.10$   & $30.72\pm1.55$   & $4.29\pm0.60$   & $40.90\pm1.71$  \\ \hline
SR3b & $0.54\pm0.14$   & $0.17\pm0.04$   & $3.31\pm0.51$   & $0.59\pm0.09$   & $4.63\pm0.53$  \\ \hline
SR4a & $1.59\pm0.21$   & $0.75\pm0.08$   & $12.12\pm0.97$   & $0.75\pm0.13$   & $15.22\pm1.01$  \\ \hline
SR4b & $0.16\pm0.09$   & $0.12\pm0.03$   & $0.87\pm0.26$   & $0.14\pm0.10$   & $1.29\pm0.30$  \\ \hline
SR5 & $41.56\pm0.95$   & $20.87\pm0.40$   & $16.16\pm1.12$   & $3.14\pm0.78$   & $81.73\pm1.71$  \\ \hline
SR6 & $9.93\pm0.47$   & $4.18\pm0.18$   & $3.57\pm0.53$   & $0.63\pm0.13$   & $18.32\pm0.74$  \\ \hline
SR7 & $13.21\pm0.60$   & $3.59\pm0.16$   & $3.91\pm0.56$   & $0.68\pm0.15$   & $21.40\pm0.85$  \\ \hline
SR8 & $3.59\pm0.31$   & $1.19\pm0.10$   & $1.48\pm0.35$   & $0.14\pm0.04$   & $6.40\pm0.48$  \\ \hline
SR9 & $27.79\pm0.75$   & $9.81\pm0.27$   & $1.63\pm0.34$   & $0.55\pm0.09$   & $39.78\pm0.87$  \\ \hline
SR10 & $4.34\pm0.30$   & $1.28\pm0.10$   & $0.14\pm0.10$   & $0.10\pm0.04$   & $5.86\pm0.33$  \\ \hline
SR11 & $8.00\pm0.45$   & $1.69\pm0.11$   & $0.17\pm0.12$   & $0.16\pm0.07$   & $10.01\pm0.48$  \\ \hline
SR12 & $1.58\pm0.21$   & $0.47\pm0.05$   & $0.15\pm0.11$   & $0.01\pm0.01$   & $2.21\pm0.24$  \\ \hline
SR13 & $3.37\pm0.27$   & $0.70\pm0.06$   & $0.06\pm0.06$   & $0.02\pm0.01$   & $4.15\pm0.28$  \\ \hline
SR14a & $10.30\pm0.65$   & $2.08\pm0.12$   & $11.29\pm0.94$   & $5.07\pm1.09$   & $28.74\pm1.58$  \\ \hline
SR14b & $1.53\pm0.26$   & $0.53\pm0.06$   & $1.76\pm0.37$   & $0.54\pm0.12$   & $4.36\pm0.47$  \\ \hline
SR15a & $4.41\pm0.40$   & $1.33\pm0.10$   & $7.62\pm0.77$   & $1.10\pm0.15$   & $14.46\pm0.89$  \\ \hline
SR15b & $0.69\pm0.18$   & $0.45\pm0.06$   & $0.49\pm0.20$   & $0.15\pm0.05$   & $1.78\pm0.28$  \\ \hline
SR16a & $3.23\pm0.28$   & $0.87\pm0.08$   & $13.30\pm1.02$   & $1.37\pm0.21$   & $18.77\pm1.08$  \\ \hline
SR16b & $0.79\pm0.13$   & $0.34\pm0.05$   & $0.70\pm0.23$   & $0.24\pm0.06$   & $2.07\pm0.28$  \\ \hline \hline

\end{tabular}

\end{center}
\end{table}

\begin{table}[tbh!]
\begin{center}
\caption{Simulated yields for 35.9 \fbinv of the rare \acs{SM} backgrounds in the $23$ off-Z signal regions and the associated statistical uncertainty from the size of the samples. Shown uncertainty is \acs{MC} statistics.}
\label{tab:SUSYMCyieldsOFFZ}

\begin{tabular}{||c||c|c|c|c|c|}\hline

SR1a & $9.78\pm0.47$   & $16.65\pm0.57$   & $49.35\pm1.95$   & $18.68\pm3.06$   & $94.47\pm3.70$  \\ \hline
SR1b & $0.14\pm0.05$   & $0.25\pm0.06$   & $0.30\pm0.15$   & $0.05\pm0.06$   & $0.74\pm0.18$  \\ \hline
SR2a & $1.26\pm0.19$   & $3.90\pm0.26$   & $5.47\pm0.64$   & $1.67\pm0.38$   & $12.32\pm0.81$  \\ \hline
SR2b & $0.14\pm0.06$   & $0.19\pm0.04$   & $0.07\pm0.07$   & $0.14\pm0.06$   & $0.54\pm0.12$  \\ \hline
SR3a & $1.60\pm0.21$   & $1.80\pm0.17$   & $4.47\pm0.59$   & $2.45\pm0.92$   & $10.32\pm1.13$  \\ \hline
SR3b & $0.05\pm0.04$   & $0.03\pm0.01$   & $0.10\pm0.08$   & $0.01\pm0.01$   & $0.19\pm0.09$  \\ \hline
SR4a & $0.62\pm0.13$   & $1.21\pm0.13$   & $2.00\pm0.40$   & $0.69\pm0.19$   & $4.52\pm0.48$  \\ \hline
SR4b & $0.02\pm0.02$   & $0.05\pm0.01$   & $0.05\pm0.05$   & $0.07\pm0.07$   & $0.19\pm0.09$  \\ \hline
SR5  & $17.64\pm0.62$   & $32.33\pm0.77$   & $3.99\pm0.56$   & $3.37\pm1.01$   & $57.33\pm1.52$  \\ \hline
SR6  & $3.24\pm0.26$   & $6.81\pm0.31$   & $0.65\pm0.23$   & $0.05\pm0.13$   & $10.75\pm0.49$  \\ \hline
SR7  & $3.24\pm0.29$   & $4.62\pm0.25$   & $0.62\pm0.22$   & $0.38\pm0.29$   & $8.85\pm0.53$  \\ \hline
SR8  & $1.27\pm0.17$   & $2.48\pm0.18$   & $0.39\pm0.17$   & $0.06\pm0.04$   & $4.20\pm0.30$  \\ \hline
SR9  & $8.84\pm0.40$   & $15.46\pm0.48$   & $0.14\pm0.10$   & $0.01\pm0.06$   & $24.45\pm0.64$  \\ \hline
SR10 & $1.56\pm0.16$   & $2.94\pm0.18$   & $0.00\pm0.00$   & $0.00\pm0.00$   & $4.49\pm0.25$  \\ \hline
SR11 & $1.41\pm0.23$   & $2.58\pm0.20$   & $0.00\pm0.00$   & $0.01\pm0.01$   & $4.00\pm0.30$  \\ \hline
SR12 & $0.67\pm0.12$   & $1.21\pm0.10$   & $0.07\pm0.07$   & $0.00\pm0.00$   & $1.95\pm0.17$  \\ \hline
SR13 & $0.66\pm0.13$   & $1.44\pm0.15$   & $0.00\pm0.00$   & $0.01\pm0.01$   & $2.11\pm0.20$  \\ \hline
SR14a & $3.07\pm0.33$   & $4.88\pm0.26$   & $1.60\pm0.35$   & $0.83\pm0.49$   & $10.38\pm0.73$  \\ \hline
SR14b & $0.01\pm0.06$   & $0.08\pm0.05$   & $0.16\pm0.11$   & $0.06\pm0.04$   & $0.28\pm0.14$  \\ \hline
SR15a & $1.65\pm0.23$   & $3.77\pm0.22$   & $1.56\pm0.35$   & $0.41\pm0.16$   & $7.39\pm0.50$  \\ \hline
SR15b & $0.03\pm0.05$   & $0.21\pm0.05$   & $0.08\pm0.08$   & $0.08\pm0.05$   & $0.40\pm0.12$  \\ \hline
SR16a & $1.00\pm0.16$   & $2.80\pm0.20$   & $1.48\pm0.34$   & $0.92\pm0.18$   & $6.19\pm0.46$  \\ \hline
SR16b & $0.13\pm0.05$   & $0.19\pm0.04$   & $0.00\pm0.00$   & $0.06\pm0.04$   & $0.37\pm0.08$  \\ \hline

\end{tabular}

\end{center}
\end{table}